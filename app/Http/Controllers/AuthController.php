<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register ()
    {
        return view('register');
    }

    public function welcome (Request $request)
    {   
        $name = $request->name;
        $Lname = $request->Lname;

        if($Lname == null){
            $Lname = " ";
        }

        $Fname = $name . " " . $Lname;
        return view('berhasil', [
            'name' => $Fname
        ]);
    }
}
