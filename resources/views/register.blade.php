<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>

<body>
    <div>
        <h1>Buat Account Baru</h1>

        <h2>Sign Up Form</h2>
    </div>
    <form action="{{url('/welcome')}}" method="POST">
        @csrf
        <div>
            <div>
                <div>
                    <label for="Fname">First name:</label>
                    <br><br>
                    <input type="text" id="Fname" name="name">
                </div>
                <br>
                <div>
                    <label for="Lname">Last name:</label>
                    <br><br>
                    <input type="text" id="Lname" name="Lname">
                </div>
            </div>
            <div>
                <p>Gender:</p>
                <div>
                    <div>
                        <input type="radio" id="Male" name="gender">
                        <label for="Male">Male</label>
                    </div>

                    <div>
                        <input type="radio" id="Female" name="gender">
                        <label for="Female">Female</label>
                    </div>

                    <div>
                        <input type="radio" id="Other" name="gender">
                        <label for="Other">Other</label>
                    </div>
                </div>
            </div>

            <br>

            <div>

                <label for="Nationality">Nationality</label>
                <br>
                <br>
                <div>
                    <select name="Nationality" id="Nationality">
                        <option value="indonesia">indonesia</option>
                        <option value="malaysia">malaysia</option>
                        <option value="singapore">singapore</option>
                    </select>
                </div>
            </div>

            <div>
                <p>Language Spoken:</p>
                <div>
                    <input type="checkbox" id="bindo" name="bindo" value="Bike">
                    <label for="bindo"> Bahasa Indonesia</label><br>
                </div>
                <div>
                    <input type="checkbox" id="English" name="English" value="Bike">
                    <label for="English"> English</label><br>
                </div>
                <div>
                    <input type="checkbox" id="other" name="other" value="Bike">
                    <label for="other"> Other</label><br>
                </div>

            </div>

            <div>
                <p>Bio:</p>

                <div>
                    <textarea id="w3review" name="w3review" rows="10" cols="30"></textarea>
                </div>
            </div>

            <div>
                <button type="submit">Sign Up</button>
            </div>
    </form>

    </div>

</body>

</html>
